import { useEffect } from "react";
import { gsap } from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";
import '../styles/css/App.css'
import Header from './Header'
import MainContent from './MainContent'

const App = () => {

  const handleClickMenu = () => {
    const trigger = document.querySelector("menu > .trigger");
    trigger.parentElement.classList.toggle("open");
  }

  useEffect(() => {
    gsap.registerPlugin(ScrollTrigger);

    let tl = gsap.timeline({ defaults: { duration: 1, ease: "ease" } })

    // HEADER
    tl.fromTo('.link_menu', { opacity: 0, y: -200 }, { opacity: 1, y: 0, stagger: .1, delay: .3 })
      .fromTo("#description_title", { opacity: 0 }, { opacity: 1 })
      .fromTo("#description_title", { y: -50 }, { y: 0 })
      .fromTo("#hero_image", { opacity: 0, x: 1000 }, { opacity: 1, x: 0 }, "-=.7")
      .fromTo("#hero_image", { scale: 1.2 }, { scale: 1 }, "-=.5")
      .fromTo("#features_title", { opacity: 0 }, { opacity: 1 }, "-=.5")
      .fromTo("#features_title", { y: -50 }, { y: 0 }, "-=.5")

    // TITLE
    gsap.timeline({
      defaults: { duration: .5, ease: "ease" },
      scrollTrigger: {
        trigger: "#download_section",
        start: "top 80%"
      }
    }).fromTo("#title_download_section", { opacity: 0 }, { opacity: 1 })
      .fromTo("#title_download_section", { y: -50 }, { y: 0 })

    gsap.timeline({
      defaults: { duration: .5, ease: "ease" },
      scrollTrigger: {
        trigger: "#faq_section",
        start: "top 80%"
      }
    }).fromTo("#title_faq_section", { opacity: 0 }, { opacity: 1 })
      .fromTo("#title_faq_section", { y: -50 }, { y: 0 })

    // TAB CONTENT
    const tlTabContent = gsap.timeline({ defaults: { duration: 1, ease: "ease" } })
    document.querySelector('#tabs').addEventListener('click', () => {
      tlTabContent.fromTo('#image_tab_content', { opacity: 0, x: -1500 }, { opacity: 1, x: 0, duration: 1.5 })
        .fromTo('#title_tab_content', { opacity: 0, x: 200 }, { opacity: 1, x: 0 }, "-=1")
        .fromTo('#text_tab_content', { opacity: 0, x: 200 }, { opacity: 1, x: 0 }, "-=.7")
    })

  }, [])

  return (

    <main className="App">

      <Header />
      <MainContent />

      <menu id="attribution_menu">
        <a href="https://bit.ly/3wNmmu9" className="action" title="Malt"><i className="fa-solid fa-m"></i></a>
        <a href="https://bit.ly/3NB1j4x" className="action" title="Twitter"><i className="fab fa-twitter"></i></a>
        <a href="https://bit.ly/3LOCAbR" className="action" title="Facebook"><i className="fab fa-facebook-f"></i></a>
        <a href="https://bit.ly/3MKUAoN" className="action" title="LinkedIn"><i className="fab fa-linkedin-in"></i></a>
        <div className="trigger" title="Valentin Laidet | Développeur Web & Mobile" onClick={handleClickMenu}></div>
      </menu>

    </main>
  );
}

export default App;
