import React, { useRef, useState, useEffect } from 'react'
import '../styles/css/MainContent.css'

// Tab Images
import ImageFirstTab from '../images/illustration-features-tab-1.svg'
import ImageSecondTab from '../images/illustration-features-tab-2.svg'
import ImageThirdTab from '../images/illustration-features-tab-3.svg'

// Browser Images
import ChromeImage from '../images/logo-chrome.svg'
import FirefoxImage from '../images/logo-firefox.svg'
import OperaImage from '../images/logo-opera.svg'

import DotsImage from '../images/bg-dots.svg'
import { ReactComponent as ArrowIcon } from '../images/icon-arrow.svg'

// Footer Images
import BookmarkLogo from '../images/logo-bookmark-text-white.svg'
import { ReactComponent as ErrorIcon } from '../images/icon-error.svg'
import { ReactComponent as FacebookIcon } from '../images/icon-facebook.svg'
import { ReactComponent as TwitterIcon } from '../images/icon-twitter.svg'

const MainContent = () => {

    const emailInputEl = useRef(null)
    const [emailValid, setEmailValid] = useState(null)
    const [activedTab, setActivedTab] = useState(0)

    const onContactUsClicked = () => {
        var value = emailInputEl.current.value
        const emailPattern = new RegExp("^[a-zA-Z0-9._:$!%-]+@[a-zA-Z0-9.-]+.[a-zA-Z]$")
        if (emailPattern.test(value)) {
            setEmailValid(true)
        } else {
            if (emailValid || emailValid === null) {
                setEmailValid(false)
            }
        }
    }

    const contentTabs = [
        {
            id: 0,
            image: ImageFirstTab,
            title: "Bookmark in one click",
            text: "Organize your bookmarks however your like. Our simple drag-and-drop interface gives you complete control over how you manage your favourite sites."
        },
        {
            id: 1,
            image: ImageSecondTab,
            title: "Intelligent search",
            text: "Our powerful search feature will help you find saved sites in no time at all. No need to trawl through all of your bookmarks."
        },
        {
            id: 2,
            image: ImageThirdTab,
            title: "Share your bookmarks",
            text: "Easily share your bookmarks and collections with others. Create a shareable link that you car sent at the click of a button."
        }
    ]
    const activeContentTab = contentTabs.find(el => el.id === activedTab);

    const downloadCards = [
        {
            id: 1,
            image: ChromeImage,
            name: "Chrome",
            version: 62
        },
        {
            id: 2,
            image: FirefoxImage,
            name: "Firefox",
            version: 55
        },
        {
            id: 3,
            image: OperaImage,
            name: "Opera",
            version: 46
        },
    ]

    const accordionDatas = [
        {
            id: 1,
            title: "What is Bookmark ?",
            content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt justo eget ultricies fringilla. Phasellus blandit ipsum quis quam ornare mattis."
        },
        {
            id: 2,
            title: "How can I request a new browser?",
            content: "Vivamus luctus eros aliquet convallis ultricies. Mauris augue massa, ultricies non ligula. Suspendisse imperdiet. Vivamus luctus eros aliquet convallis ultricies. Mauris augue massa, ultricies non ligula. Suspendisse imperdie tVivamus luctus eros aliquet convallis ultricies. Mauris augue massa, ultricies non ligula. Suspendisse imperdiet."
        },
        {
            id: 3,
            title: "Is there a mobile app?",
            content: "Sed consectetur quam id neque fermentum accumsan. Praesent luctus vestibulum dolor, ut condimentum urna vulputate eget. Cras in ligula quis est pharetra mattis sit amet pharetra purus. Sed sollicitudin ex et ultricies bibendum."
        },
        {
            id: 4,
            title: "What about other Chromium browsers?",
            content: "Integer condimentum ipsum id imperdiet finibus. Vivamus in placerat mi, at euismod dui. Aliquam vitae neque eget nisl gravida pellentesque non ut velit."
        }
    ]

    const [activedAccordion, setActivedAccordion] = useState(null)
    const handleClickAccordion = (id) => {
        if (activedAccordion === id) {
            setActivedAccordion(null)
        } else {
            setActivedAccordion(id)
        }
    }

    return (
        <article id="main_content">

            <section id="features_section">

                <h2 id="features_title" className="title">Features</h2>
                <p id="features_text">Our aim is to make it quick and easy for you to access your favourite websites.Your bookmarks sync between your devices so you can access them on the go.</p>

                <div id="tabs">
                    <p className={activedTab === 0 ? "tab active" : "tab"} onClick={() => setActivedTab(0)}>Simple Bookmarking</p>
                    <p className={activedTab === 1 ? "tab active" : "tab"} onClick={() => setActivedTab(1)}>Speedy Searching</p>
                    <p className={activedTab === 2 ? "tab active" : "tab"} onClick={() => setActivedTab(2)}>Easy Sharing</p>
                </div>

                <div id="container_tab_content">

                    <div id="content_tab_content">
                        <div id="container_image_tab_content">
                            <img id="image_tab_content" src={activeContentTab.image} alt="tab content" />
                        </div>
                        <div id="container_text_tab_content">
                            <h3 id="title_tab_content">{activeContentTab.title}</h3>
                            <p id="text_tab_content">{activeContentTab.text}</p>
                            <button id="button_text_tab_content">More info</button>
                        </div>
                    </div>

                </div>
            </section>

            <section id="download_section">
                <h4 id="title_download_section" className="title">Download the extension</h4>
                <p id="text_download_section">We've got more browsers in the pipeline. Please do let us know if you've got a favourite you'd like us to prioritize.</p>

                <div id="container_cards_download_section">
                    {downloadCards.map(card => (
                        <div key={card.id} className="card">
                            <img className="image_card" src={card.image} alt={"Navigateur " + card.name} />
                            <p className="title_card">Add to {card.name}</p>
                            <p className="version_card">Minimum version {card.version}</p>
                            <img className="image_dots_card" src={DotsImage} alt="dots" />
                            <button className="button_card">Add & Install Extension</button>
                        </div>
                    ))}
                </div>
            </section>

            <section id="faq_section">
                <h5 id="title_faq_section" className="title">Frequently Asked Questions</h5>
                <p id="text_faq_section">Here are some of our FAQs. If your have any other questions you'd like answered please feel free to email us.</p>
                <div id="container_questions">
                    {accordionDatas.map(el => (
                        <div key={el.id} className={activedAccordion === el.id ? "accordion active" : "accordion"}>
                            <div className="accordion_item" onClick={() => handleClickAccordion(el.id)}>
                                <span className="title_accordion">{el.title}</span>
                                <ArrowIcon className="arrow_icon_accordion" />
                            </div>


                            {activedAccordion !== null && activedAccordion === el.id && (
                                <div className="accordion_content">
                                    <span className="text_accordion_content">{el.content}</span>
                                </div>
                            )}
                        </div>
                    ))}

                    <button className="button_container_questions">More Info</button>
                </div>
            </section>

            <section id="above_footer_section">
                <p id="above_title_above_footer_section">35,000 + already joined</p>
                <p id="title_above_footer_section">Stay up-to-date with what we're doing</p>

                <form id="form_above_footer_section" action="#" method="post" onSubmit={e => e.preventDefault()}>

                    <div id="input_wrapper_above_footer_section">
                        <input id="input_above_footer_section" className={!emailValid && emailValid !== null ? "error_input" : ""} type="email" placeholder="Enter your email address" ref={emailInputEl} />
                        {!emailValid && emailValid !== null && (
                            <>
                                <ErrorIcon id="icon_error_input" />
                                <span id="error_input_above_footer_section">Whoops, make sure it's an email</span>
                            </>
                        )}
                    </div>
                    <button id="button_above_footer_section" type="submit" onClick={onContactUsClicked}>Contact Us</button>

                </form>
            </section>

            <footer id="footer">

                <div id="left">
                    <img id="logo_footer" src={BookmarkLogo} alt="bookmark logo" />
                    <ul id="menu_footer">
                        <li>features</li>
                        <li>pricing</li>
                        <li>contact</li>
                    </ul>
                </div>

                <div id="container_social_medias">
                    <FacebookIcon className="social_media" />
                    <TwitterIcon className="social_media" />
                </div>
            </footer>

        </article >
    )
}

export default MainContent