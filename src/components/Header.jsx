import React, { useState, useEffect } from 'react'
import { gsap } from "gsap";
import '../styles/css/Header.css'

// SVGs
import BookmarkLogo from '../images/logo-bookmark.svg'
import BookmarkWhiteLogo from '../images/logo-bookmark-white.svg'
import CloseIcon from '../images/icon-close.svg'
import FacebookIcon from '../images/icon-facebook.svg'
import TwitterIcon from '../images/icon-twitter.svg'
import HamburgerIcon from '../images/icon-hamburger.svg'
import HeroImage from '../images/illustration-hero.svg'

const Header = () => {

    const MAX_MOBILE_WIDTH = 1000;
    const pages = ["features", "pricing", "contact"]

    const [isMobile, setIsMobile] = useState(null)
    const [menuIsOpen, setMenuIsOpen] = useState(false)

    const handleClickMenu = () => {
        setMenuIsOpen(!menuIsOpen)
    }

    useEffect(() => {
        if (isMobile) {
            let tl = gsap.timeline({ defaults: { duration: .3, ease: "ease" } })
            if (menuIsOpen) {
                document.querySelector('body').style.overflowY = 'hidden'
                tl.fromTo('#overlay', { scaleY: .01, scaleX: 0, opacity: 0, display: "none" }, { scaleX: 1, opacity: 1, display: "block" })
                    .to("#overlay", { scaleY: 1 })
                    .fromTo(".link_mobile_menu", { opacity: 0 }, { opacity: 1, stagger: .3 })
            } else {
                document.querySelector('body').style.overflowY = 'scroll'
                tl.fromTo(".link_mobile_menu", { opacity: 1 }, { opacity: 0, stagger: { from: "end", each: .3 } })
                    .fromTo('#overlay', { scaleY: 1, scaleX: 1, opacity: 1, display: "block" }, { scaleY: .01 })
                    .to("#overlay", { scaleX: 0, opacity: 0, display: "none" })
            }
        }
    }, [menuIsOpen])

    useEffect(() => {
        if (window.innerWidth <= MAX_MOBILE_WIDTH) {
            setIsMobile(true)
        } else {
            setIsMobile(false)
        }
    }, [])

    window.onresize = () => {
        if (isMobile !== null && window.innerWidth <= MAX_MOBILE_WIDTH) {
            if (!isMobile) {
                setIsMobile(true)
            }
        } else {
            if (isMobile) {
                setIsMobile(false)
            }
        }
    }

    return (

        <header id="header">

            {isMobile && (
                <section id="menu_mobile">
                    {!menuIsOpen && (
                        <>
                            <img id="menu_logo" src={BookmarkLogo} alt="bookmark logo" />
                            <img id="menu_toggle" src={HamburgerIcon} alt="hamburger icon" onClick={handleClickMenu} />
                        </>
                    )}

                    <div id="overlay" style={{ opacity: 0, display: "none" }}>

                        <div id="content_mobile_menu">
                            <div id="header_mobile_menu">
                                <img id="logo" src={BookmarkWhiteLogo} alt="bookmark logo" />
                                <img id="close" src={CloseIcon} alt="close" onClick={handleClickMenu} />
                            </div>

                            <nav id="navigation_mobile_menu">
                                <ul id="links_mobile_menu">
                                    {pages.map(page => (
                                        <li key={page} className="link_mobile_menu">{page}</li>
                                    ))}
                                </ul>
                            </nav>

                            <button id="button_mobile_menu">Login</button>

                            <div id="container_social_medias">
                                <img src={FacebookIcon} alt="facebook" className="social_media" />
                                <img src={TwitterIcon} alt="twitter" className="social_media" />
                            </div>
                        </div>

                    </div>
                </section>
            )}

            {!isMobile && (
                <section id="menu">
                    <img id="menu_logo" src={BookmarkLogo} alt="bookmark logo" />

                    <div id="right_side">
                        <nav id="navigation_menu">
                            <ul id="links_menu">
                                {pages.map(page => (
                                    <a key={page} className="link_menu" href="/">
                                        <li>{page}</li>
                                    </a>
                                ))}
                            </ul>
                        </nav>
                        <button id="button_menu">Login</button>
                    </div>
                </section>
            )}

            <section id="hero_section">

                <div id="content_hero_section">
                    <div id="container_hero_image">
                        <img id="hero_image" src={HeroImage} alt="hero" />
                    </div>

                    <div id="container_hero_description">
                        <h1 id="description_title">A simple Bookmark Manager</h1>
                        <p id="text">A clean simple interface to organize your favourite websites. Open a new browser tab and see your sites load instantly. Try it for free.</p>

                        <div id="container_button">
                            <button id="chrome_button">Get it on Chrome</button>
                            <button id="firefox_button">Get it on Firefox</button>
                        </div>
                    </div>
                </div>

            </section>

        </header>

    )
}

export default Header